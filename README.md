# Gitlab CICD Docker EC2

- Hanya untuk test.
- Diasumsikan sudah ada EC2 Instance running di Public subnet dan terinstall docker
  - Provisioning Instance dengan terraform [disini](https://gitlab.com/Adityacprtm/terraform-ec2), jika belum
- Diasumsikan sudah tersedia Personal Access Token

## Case

- Build Docker Image nginx
- Deploy to EC2 Server

## How To

- Setup Env Var di `Gitlab settings - CICD`

  ```sh
  EC2_USER="xxx" # Variable
  EC2_HOST="xxx" # Variable
  EC2_SSH_KEY="---RSA---" # Variable | pem file

  CI_REGISTRY_USER="xxx"
  CI_REGISTRY_TOKEN="xxx123"
  ```

- Run Pipeline manually
