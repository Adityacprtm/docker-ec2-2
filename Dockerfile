FROM    nginx:1.21.4-alpine
COPY    hello.txt /var/www/hello.txt
COPY    default.conf /etc/nginx/conf.d/default.conf
EXPOSE  80
CMD     ["nginx", "-g", "daemon off;"]
